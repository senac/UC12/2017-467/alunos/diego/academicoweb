
<%@page import="br.com.senac.agenda.model.UsuarioCad"%>
<jsp:include page="../header.jsp"/>

<%
    
    UsuarioCad usuario = (UsuarioCad) request.getAttribute("usuario");
    String mensagem = (String) request.getAttribute("mensagem");
    String erro = (String) request.getAttribute("erro");

%>

<% if (mensagem != null) {%>

<div class="alert alert-success" role="alert">
    
    <%= mensagem%>

</div>

<%}%>

<% if (erro != null) {%>

<div class="alert alert-danger" role="alert">
  
    <%= erro%>
</div>

<%}%>

<form action="./SalvarUsuarioServlet" method="post">

 <div class="form-group">
    
        <label for="codigo">C�digo</label>
        <input type="text" class="form-control col-2" id="codigo" name="codigo" readonly value="<%= usuario != null ? usuario.getId() : "" %>" >
    
    </div>
    
    <div class="form-group">
        
        <label for="nome">Nome</label>
        <input type="text" class="form-control" id="nome" placeholder="nome" name="nome" value="<%= usuario != null ? usuario.getNome() : "" %>" >
    
    </div>
    
    <div class="row">
        
        <div class="col-4"
        <label for="telefone">Telefone</label>
        <input type="text" class="form-control col-15" id="telefone" placeholder="telefone" name="telefone">
        </div>
        
        <div class="col-4">
            
        <label for="celular">Celular</label>
        <input type="text" class="form-control col-15" id="celular" placeholder="celular" name="celular">
              
        </div>
        
        <div class="col-4">
        <label for="fax">Fax</label>
        <input type="text" class="form-control col-15" id="fax" placeholder="fax" name="fax"> 
        </div>
        
    </div>

    
    <div class="row">
       
        <div class="col-3"> 
        
        <label for="cep">CEP</label>
        <input type="text" class="form-control col-15" id="cep" placeholder="cep" name="cep">
         
        </div>
    
        
        <div class="col-7">
       
        <label for="endereco">Endereco</label>
        <input type="text" class="form-control col-15" id="endereco" placeholder="endereco" name="endereco">
    
         </div>
        
        <div class="col-2">
       
        <label for="Numero">N�mero</label>
        <input type="text" class="form-control col-15" id="numero" placeholder="numero" name="numero">
    
    </div>
        
        
    </div>

    <div class="row">
       
        <div class="col-4"
             
        <label for="bairro">Bairro</label>
        <input type="text" class="form-control col-15" id="bairro" placeholder="bairro" name="bairro">
        
        </div>
       
    
    <div class="col-6">
       
        <label for="cidade">Cidade</label>
        <input type="text" class="form-control col-15" id="cidade" placeholder="cidade" name="cidade">
    
    </div>
    
    <div class="col-2">
       
        <label for="uf">UF</label>
        <select name="Estado"class="form-control col-15" id="uf" placeholder="uf" name="uf">
                <option value="">AC</option>
                <option value="saab">AL</option>
                <option value="fiat">AM</option>
                <option value="fiat">BA</option>
                <option value="fiat">CE</option>
                <option value="fiat">DF</option>
                <option value="fiat">ES</option>
                <option value="fiat">GO</option>
                <option value="fiat">MA</option>
                <option value="fiat">MT</option>
                <option value="fiat">MS</option>
                <option value="fiat">MG</option>
                <option value="fiat">PA</option>
                <option value="fiat">PB</option>
                <option value="fiat">PR</option>
                <option value="fiat">PE</option>
                <option value="fiat">PI</option>
                <option value="fiat">RJ</option>
                <option value="fiat">RN</option>
                <option value="fiat">RS</option>
                <option value="fiat">RR</option>
                <option value="fiat">SC</option>
                <option value="fiat">SP</option>
                <option value="fiat">SE</option>
                <option value="fiat">TO</option>
                
             
            </select>  
    </div>
        
        
        </div>
    
    <div class="row">

        <div class="col-12">
       
        <label for="email">E-mail</label>
        <input type="text" class="form-control col-15" id="email" placeholder="email" name="email">
        </div>
    </div>
    
    <div class="form-group text-right">
      
        <div>
        <button type="submit" class="btn btn-primary">Salvar</button>
        <button type="reset" class="btn btn-danger">Cancelar</button>
   
        </div>
    </div>
        
        </form>

<jsp:include page="../footer.jsp"/>
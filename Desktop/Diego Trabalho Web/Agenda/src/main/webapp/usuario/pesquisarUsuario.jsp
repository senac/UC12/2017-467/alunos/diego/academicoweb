
<%@page import="br.com.senac.agenda.model.UsuarioCad"%>
<%@page import="java.util.List"%>
<jsp:include page="../header.jsp"/>

<% List<UsuarioCad> lista = (List) request.getAttribute("lista"); %> 

<% String mensagem = (String) request.getAttribute("mensagem"); %>

<% if (mensagem != null) {%>

<div class="alert alert-danger">
    <%= mensagem%>
</div>

<% } %>

<fieldset  >
    <legend>Pesquisa de Contatos Cadastrados</legend>
    <form class="form-inline" action="./PesquisaUsuarioServlet">
        
        <div class="form-group" style="padding: 20px;">
            <label for="txtCodigo" style="margin-right: 10px">C�digo:</label> 
            <input id="id" name="id" class="form-control form-control-sm" id="txtCodigo" type="text" />
            
        </div>

        <div class="form-group" style="padding: 20px;">
            <label for="nome" style="margin-right: 10px">Nome:</label> 
            <input name="nome" id="nome" class="form-control form-control-sm" type="text" />
        </div>

        <div class="form-group" style="padding: 20px;">
            <label for="estado" style="margin-right: 10px">Estado:</label> 
                <select name="Estado" id="uf" class="form-control form-control-sm" type="text" >
                <option value="">AC</option>
                <option value="saab">AL</option>
                <option value="fiat">AM</option>
                <option value="fiat">BA</option>
                <option value="fiat">CE</option>
                <option value="fiat">DF</option>
                <option value="fiat">ES</option>
                <option value="fiat">GO</option>
                <option value="fiat">MA</option>
                <option value="fiat">MT</option>
                <option value="fiat">MS</option>
                <option value="fiat">MG</option>
                <option value="fiat">PA</option>
                <option value="fiat">PB</option>
                <option value="fiat">PR</option>
                <option value="fiat">PE</option>
                <option value="fiat">PI</option>
                <option value="fiat">RJ</option>
                <option value="fiat">RN</option>
                <option value="fiat">RS</option>
                <option value="fiat">RR</option>
                <option value="fiat">SC</option>
                <option value="fiat">SP</option>
                <option value="fiat">SE</option>
                <option value="fiat">TO</option> 
             
            </select>   
        </div>

            <button style="margin-right: 60px" type="submit" class="btn btn-default" >
                Pesquisar
            </button>
            
    </form>
</fieldset>

<hr />

<table class="table table-hover">
    <thead>
        <tr>
            <th>C�digo</th> <th>Nome</th> <th>Endere�o</th> <th>Telefone</th> <th>Celular</th> <th>Fax</th> <th>E-mail</th>
        </tr>

    </thead>

    <% if (lista != null && lista.size() > 0) {
            for (UsuarioCad u : lista) {
    %>
    <tr>
        <td><%= u.getId()%></td> <td><%= u.getNome()%></td> <td><%= u.getEndereco() + ", " + u.getNumero() + ", " + u.getBairro() + ", " + u.getCidade() + " - " + u.getUf()%></td> <td><%= u.getTelefone()%></td> <td><%= u.getCelular()%></td> <td><%= u.getFax()%></td><td><%= u.getEmail()%></td
    </tr>

    <% } // for

    } else {%>

    <tr >
        <td  colspan="2">N�o Existem registros.</td>
    </tr>

    <%}%>


</table>

<jsp:include page="../footer.jsp"/>